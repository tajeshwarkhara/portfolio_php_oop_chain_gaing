<?php

class Bicycle
{
    public const CATEGORIES = ["Road","Mountain","Hybrid","Cruiser","City","BMX"];

    public const GENDERS = ["mens","womens","unisex"];

    protected const CONDITION_OPTIONS = [
        1 => 'Beat up',
        2 => 'Decent',
        3 => 'Good',
        4 => 'Great',
        5 => 'Like new'
    ];

    public $brand;
    public $model;
    public $year;
    public $category;
    public $color;
    public $description;
    public $gender;
    public $price;
    protected $weight_kg;
    protected $condition_id;

    // Constructor with arguments array
    public function __construct($args = [])
    {
        $this->brand = $args['brand'] ?? '';
        $this->model = $args['model'] ?? '';
        $this->year = $args['year'] ?? '';
        $this->category = $args['category'] ?? '';
        $this->color = $args['color'] ?? '';
        $this->description = $args['description'] ?? '';
        $this->gender = $args['gender'] ?? '';
        $this->price = $args['price'] ?? 0;
        $this->weight_kg = $args['weight_kg'] ?? 0.0;
        $this->condition_id = $args['condition_id'] ?? 3;

        // Another common technique
        // Caution: allows private/protected properties to be set
        /*foreach ($args as $k => $v)
        {
            if(property_exists($this, $k))
            {
                $this->$k = $v;
            }
        }*/
    }

    // Get weight_kg
    public function get_weight_kg()
    {
        return number_format($this->weight_kg, 2) . ' kg';
    }

    // Set weight_kg
    public function set_weight_kg($weight_kg_in)
    {
        $this->weight_kg = floatval($weight_kg_in);
    }

    // Get weight lbs
    public function get_weight_lbs()
    {
        $weight_lbs = floatval($this->weight_kg) * 2.204;
        return number_format($weight_lbs, 2) . ' lbs';
    }

    //  Set weight lbs
    public function set_weight_pounds($weight_lbs_in)
    {
        $this->weight_kg = floatval($weight_lbs_in)/2.204;
    }

    // Return condition
    public function condition()
    {
        if($this->condition_id > 0)
        {
            return self::CONDITION_OPTIONS[$this->condition_id];
        }
        else
        {
            return "Unknown";
        }
    }


}




?>


