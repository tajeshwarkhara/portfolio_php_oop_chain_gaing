<?php

class ParseCSV
{
    public static $delimiter = ',';

    private $filename;
    private $header;
    private $data = [];
    private $row_count = 0;

    public function __construct($filename)
    {
        if($filename != '')
        {
            // Checks to see if the file name exists and is readable
            // Then assigns it to $this->filename
            $this->file($filename);
        }
    }

    // Checks if the file exists and is readable
    public function file($filename)
    {
        if(!file_exists($filename))
        {
            echo "File does not exist." . "<br>";
            return false;
        }
        elseif (!is_readable($filename))
        {
            echo "File is not readable." . "<br>";
            return false;
        }

        $this->filename = $filename;
        return true;
    }


    public function parse()
    {
        // Check to see if file name is set
        if(!isset($this->filename))
        {
            echo "File not set.";
            return false;
        }

        // Clear any previous result
        $this->reset();

        $file = fopen($this->filename, 'r');
        while(!feof($file)) {
            $row = fgetcsv($file, 0, self::$delimiter);
            if($row == [NULL] || $row === FALSE) { continue; }
            if(!$this->header) {
                $this->header = $row;
            } else {
                $this->data[] = array_combine($this->header, $row);
                $this->row_count++;
            }
        }
        fclose($file);
        return $this->data;
    }

    // Last results
    public function last_results()
    {
        return $this->data;
    }

    // Getter for row_count
    public function get_row_count()
    {
        return $this->row_count;
    }

    // Clear data
    private function reset()
    {
        $this->header = NULL;
        $this->data = [];
        $this->row_count = 0;
    }
}

?>