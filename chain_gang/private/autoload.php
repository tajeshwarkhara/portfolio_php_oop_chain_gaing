<?php

function my_autoload($class) {
    if(preg_match('/\A\w+\Z/', $class)) {
        include 'classes/' . $class . '.class.php';
    }
}
spl_autoload_register('my_autoload');

$b1 = new Bicycle([
    'brand'=>'Hero',
    'model'=>'gold line',
    'year'=>2012,
    'category'=>Bicycle::CATEGORIES[0],
    'color'=>'Red',
    'description'=>'Great bike.',
    'gender'=>Bicycle::GENDERS[0],
    'price'=> 10000
]);

echo "<pre>";
print_r($b1);
echo "</pre>";